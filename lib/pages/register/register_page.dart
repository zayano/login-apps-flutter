import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:login_apps/data/database_helper.dart';
import 'package:login_apps/models/user.dart';
import 'package:login_apps/pages/register/register_presenter.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> implements RegisterPageContract {
  BuildContext _ctx;
  bool _isLoading = false;
  bool _validateUsername = false;
  bool _validatePassword = false;
  final _textUsername = TextEditingController();
  final _textPassword = TextEditingController();
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  String _username, _password;

  RegisterPagePresenter _presenter;

  _RegisterPageState() {
    _presenter = new RegisterPagePresenter(this);
  }

  void _submit() {
    final form = formKey.currentState;

    setState(() {
      if(_textUsername.text.isEmpty){
        _textUsername.text.isEmpty ? _validateUsername = true : _validateUsername = false;
      } else if (_textPassword.text.isEmpty) {
        _textPassword.text.isEmpty ? _validatePassword = true : _validatePassword = false;
      } else {
        if (form.validate()) {
          _isLoading = true;
          form.save();
          _presenter.doRegister(_username, _password);
        }
      }
    });
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    _ctx = context;
    var registerBtn = new CupertinoButton(
        child: new Text("Register"),
        onPressed: _submit,
        color: Color.fromRGBO(0, 122, 253, 1)
    );
    var loginBtn = new CupertinoButton(child: new Text("Login"), onPressed: (){
      Navigator.of(context).pushNamed("/login");
    });

    var registerForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  controller: _textUsername,
                  onSaved: (val) => _username = val,
                  decoration: new InputDecoration(labelText: "Any Username",
                      errorText: _validateUsername ? "Username Can't be empty" : null),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  controller: _textPassword,
                  onSaved: (val) => _password = val,
                  decoration: new InputDecoration(labelText: "Any Password",
                      errorText: _validatePassword ? "Password Can't be empty" : null),
                  obscureText: true,
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: registerBtn,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: loginBtn,
        ),
        new Text(
          "Kindly provide your username and password this data can be used later to log in.",
          textScaleFactor: 1,
          textAlign: TextAlign.center,
        ),
      ],
    );


    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text("Register Page"),
//      ),
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      key: scaffoldKey,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
//              Padding(
//                padding: EdgeInsets.only(top: 20.0),
//                child: new Image.asset("assets/image_01.png",
//                ),
//              ),
              Padding(
                padding: EdgeInsets.only(bottom: 50, left: 15, right: 15),
                child: registerForm,
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
                child: Image.asset("assets/image_02.png",
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  void onRegisterError(String error) {
    // TODO: implement onLoginError
    _showSnackBar(error);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onRegisterSuccess(User user) async {
    // TODO: implement onLoginSuccess
    _showSnackBar(user.toString());
    setState(() {
      _isLoading = false;
    });
    var db = new DatabaseHelper();
    await db.saveUser(user);
    Navigator.of(context).pushNamed("/home");
  }
}
