import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:login_apps/data/database_helper.dart';
import 'package:login_apps/models/user.dart';
import 'package:login_apps/pages/login/login_presenter.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> implements LoginPageContract {
  BuildContext _ctx;
  bool _isLoading = false;
  bool _validateUsername = false;
  bool _validatePassword = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final _textUsername = TextEditingController();
  final _textPassword = TextEditingController();

  String _username, _password;

  LoginPagePresenter _presenter;

  _LoginPageState() {
    _presenter = new LoginPagePresenter(this);
  }

  void _submit() {
    final form = formKey.currentState;

      setState(() {
        if(_textUsername.text.isEmpty){
          _textUsername.text.isEmpty ? _validateUsername = true : _validateUsername = false;
        } else if (_textPassword.text.isEmpty) {
          _textPassword.text.isEmpty ? _validatePassword = true : _validatePassword = false;
        } else {
          if (form.validate()) {
            _isLoading = true;
            form.save();
            _presenter.doLogin(_username, _password);
          }
        }
      });

  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var loginBtn = new CupertinoButton(
          child: new Text("Login"),
          onPressed: _submit,
          color: Color.fromRGBO(0, 122, 253, 1)
          );
    var registerBtn = new CupertinoButton(
        child: new Text("Register"),
        onPressed: (){
      Navigator.of(context).pushNamed("/register");
    });

    var loginForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  controller: _textUsername,
                  onSaved: (val) => _username = val,
                  decoration: new InputDecoration(labelText: "Username",
                      errorText: _validateUsername ? "Username Can't be empty" : null),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextFormField(
                  controller: _textPassword,
                  onSaved: (val) => _password = val,
                  decoration: new InputDecoration(labelText: "Password",
                      errorText: _validatePassword ? "Password Can't be empty" : null),
                  obscureText: true,
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: loginBtn,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: registerBtn,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: new Text(
            "If you don't know your username and password you can always register",
            textScaleFactor: 1.0,
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );

    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      key: scaffoldKey,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: new Image.asset("assets/image_01.png",
                  ),
              ),
              SingleChildScrollView(
                padding: EdgeInsets.all(10),
                child: loginForm,
                  ),
            ],
          )
        ],
      ),
    );
  }

  @override
  void onLoginError(String error) {
    // TODO: implement onLoginError
    _showSnackBar(error);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onLoginSuccess(User user) async {
    // TODO: implement onLoginSuccess
    _showSnackBar(user.toString());
    setState(() {
      _isLoading = false;
    });
    Navigator.of(context).pushNamed("/home");
  }
}
